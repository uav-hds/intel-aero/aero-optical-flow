#you can run a prebuilt image with:
#sudo docker run -it -v ~/git/intel-aero/aero-optical-flow:/home/user/optical_flow --name aero_opticalflow -h aero_opticalflow registry.gitlab.utc.fr/uav-hds/intel-aero/aero-optical-flow:master bash
#
#otherwise build and run it:

#build container with: sudo docker build -t aero_opticalflow .
#run with: sudo docker run -it -v ~/git/intel-aero/aero-optical-flow:/home/user/optical_flow --name aero_opticalflow -h aero_opticalflow aero_opticalflow bash


FROM ubuntu:18.04

ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN useradd -ms /bin/bash user

RUN apt-get update && apt-get upgrade -y && apt-get install git libopencv-dev cmake -y

USER user
WORKDIR /home/user/optical_flow
